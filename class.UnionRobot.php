<?php
class UnionRobot
{

	public function addRobot($new, $current = []) {
		

		if(is_array($new)) {
			$current = array_merge($current, $new);
		}
		else {
			$current[] = $new;
		}

		return $current;
	}

}