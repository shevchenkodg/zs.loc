<?php
include('class.Factory.php');
include('class.UnionRobot.php');
include('class.MyHydra1.php');
include('class.MyHydra2.php');

// Обьявляем экземпляр класса Factory
$factory = new Factory();

//получаем 5 роботов типа robotsMyHydra1 согласно условиям задачи
$robotsMyHydra1 = $factory->createMyHydra1(5);
//получаем 5 роботов типа robotsMyHydra2 согласно условиям задачи
$robotsMyHydra2 = $factory->createMyHydra2(2);

// Пытаюсь разобраться с требуемым по задаче методом addRobot
$unionRobot = new UnionRobot();
// $robot1 = $unionRobot->addRobot(new MyHydra2());
$robots = $unionRobot->addRobot(new MyHydra2(), $robots);
$robots = $unionRobot->addRobot($factory->createMyHydra2(2), $robots);
$robots = $unionRobot->addRobot($factory->createMyHydra1(5), $robots);
// $robot += $unionRobot->addRobot(new MyHydra2());

echo '<meta charset="utf-8">';
echo 'мінімальна швидкість з усіх об’єднаних роботів: '.$factory->getSpeed($robots);
echo '<br>';
echo 'сума всіх ваг об’єднаних роботів: '.$factory->getWeight($robots);

/*
 * Моя думка про дане тестове завдання
 *
 * Тестове завдання дуже гарне для перевірки навичків в об'єктному PHP, а саме
 * в паттернах проектування. Я лише зміг показати тільки базові навички в об'єктному PHP.
 * Сюди дуже добре підійшов би паттерн Factory, але нажаль, як я повідомляв,
 * в моєму досвіді є лише створення сайтів, використовуючи php-фреймворки Zend Framework
 * та Laravel, тому використовувати паттерни ще не приходилось.
 * Проте, я готовий поглибити свої знання у цій сфері, якщо мені дадуть можливість та час,
 * оскільки дане завдання, на мою думку, орієнтоване на рівень вище, ніж 'Middle'.
 */